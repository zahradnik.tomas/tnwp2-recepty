function searchForRecipes(input, recipes) {
  recipes.removeClass("d-none")
  var inputValue = input.val().toLowerCase();

  if (inputValue === '') {
    $('.recipes-grid').packery()
    return
  }

  recipes.each(function () {
    let recipeName = $(this).find('h3').text().toLowerCase()
    if (!recipeName.includes(inputValue)) {
      $(this).addClass("d-none")
    }
  })
  $('.recipes-grid').packery()
}

$(document).ready(function () {
  $('#recipe-filter').on('keyup', function () {
    searchForRecipes($(this), $('.card').parent())
  })
})