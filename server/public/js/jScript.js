'use strict'

function initPackery() {
  $('.recipes-grid').packery({
    columnWidth: '.grid-sizer',
    itemSelector: '.recipe',
    percentPosition: true,
  });
}

$(document).ready(function () {
  initPackery()
});