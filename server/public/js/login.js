'use strict'

function initLoginForm() {
  let selectedRadio = $('#login-wrapper input:checked')
  if (selectedRadio.attr('id') === 'login-radio'){
    $('#register').addClass('d-none')
  } else {
    $('#login').addClass('d-none')
  }
}

  function switchForm(radio) {
    $('.btn-close').trigger('click') // Close alert

    if ($(radio).attr('for') === 'login-radio') {
      $('#register').addClass('d-none')
      $('#login').removeClass('d-none')
    } else {
      $('#login').addClass('d-none')
      $('#register').removeClass('d-none')
    }
  }

  $(document).ready(function () {
    initLoginForm()

    $('#login-wrapper label').click(function () {
      switchForm($(this))
    })
  });