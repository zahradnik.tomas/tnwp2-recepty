function _addIngredient() {
    const template = document.querySelector('#ingredient-template')
    const clon = template.content.cloneNode(true)
    $('#ingredients-list').append(clon)
}

function remove(ingredient){
    $(ingredient).parent().remove();
}

$(document).ready(function () {
    $('#add-ingredient').click(_addIngredient)
});