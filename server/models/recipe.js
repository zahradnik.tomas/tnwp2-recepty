const mongoose = require('mongoose')

const recipeSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        trim: true
    },
    difficulty: {
        type: Number,
        required: true
    },
    ingredients: {
        type: [String],
        required: true
    },
    instructions: {
        type: String,
        required: true
    },
    isPrivate: {
        type: Boolean
    },
    userId: {
        type: mongoose.Types.ObjectId,
        ref: 'User',
        required: true
    }
})

recipeSchema.methods.findDifficulty = (number) => {
    let difficulty
    switch (number) {
        case 1:
            difficulty = 'Beginner'
            break
        case 2:
            difficulty = 'Easy'
            break
        case 3:
            difficulty = 'Intermediate'
            break
        case 4:
            difficulty = 'Expert'
            break
        case 5:
            difficulty = 'Pro'
            break
        default:
            difficulty = 'This shouldn\'t happen'
    }

    return difficulty
}

const Recipe = mongoose.model('Recipe', recipeSchema)
module.exports = Recipe