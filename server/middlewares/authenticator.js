const User = require('../models/user')
const jwt = require('jsonwebtoken')
const createError = require('http-errors')

const auth = async (req, res, next) => {
    try {
        const decoded = jwt.verify(req.session.token, 'Foobar')
        const user = await User.findOne({ _id: decoded._id, 'tokens.token': req.session.token })

        if (!user) {
            throw new Error('User not found!')
        }

        req.token = req.session.token
        req.user = user

        next()
    } catch (error) {
        next(createError(403))
    }
}

module.exports = auth