const express = require('express')
const router = express.Router()
const Recipe = require('../models/recipe')
const auth = require('../middlewares/authenticator')
const bodyParser = require("body-parser")

router.use(bodyParser.json())

/* GET add recipe page. */
router.get('/add-recipe', auth, (req, res) => {
    res.render('user/add-recipe', {
        title: "Add recipe | recipe.io",
        hasToken: !!req.session.token
    })
})

/* POST add recipe request */
router.post('/add-recipe', auth, (req, res) => {
    const isPrivate = (req.body.isPrivate === 'true') // Unchecked checkbox returns undefined
    const newRecipe = new Recipe({
        name: req.body.name,
        difficulty: req.body.difficulty,
        ingredients: req.body.ingredients,
        instructions: req.body.instructions,
        isPrivate: isPrivate,
        userId: req.session.user._id
    })
    newRecipe.save().then(result => {
        res.redirect('/')
    }, error => {
        res.render('error', {
            title: error.status + ' - ' + error.message,
            hasToken: !!req.session.token
        })
    })
})

/* GET edit recipe page. */
router.get('/edit-recipe/:recipeId', auth, (req, res) => {
    Recipe.findById(req.params.recipeId).then((recipe) => {
        if (recipe.userId.toString() !== req.user._id.toString())
            return res.redirect('/')
        res.render('user/edit-recipe', {
            title: "Edit recipe | recipe.io",
            recipe: recipe,
            hasToken: !!req.session.token
        })
    }, (error) => {
        res.render('error', {
            title: error.status + ' - ' + error.message,
            hasToken: !!req.session.token
        })
    })
})

/* POST edit recipe request. */
router.post('/edit-recipe', auth, (req, res) => {
    Recipe.updateOne({_id: req.body.recipeId}, { $set: {
        name: req.body.name,
        difficulty: req.body.difficulty,
        ingredients: req.body.ingredients,
        instructions: req.body.instructions,
        isPrivate: req.body.isPrivate
    }}).then(result =>{
    res.redirect('/')
    }, (error) => {
        res.render('error', {
            title: error.status + ' - ' + error.message,
            hasToken: !!req.session.token
        })
    })
})

/* DELETE a recipe. */
router.get('/delete-recipe/:recipeId', auth, (req, res) => {
    Recipe.deleteOne({_id: req.params.recipeId, userId: req.user._id}).then(deleted => {
        res.redirect('/')
    }, error => {
        res.render('error', {
            title: error.status + ' - ' + error.message,
            hasToken: !!req.session.token
        })
    })
})

/* GET profile page redirect for logged user. */
router.get('/profile', auth, (req, res) => {
    res.redirect('profile/' + req.user.username)
})

/* GET logout page and destroy user's session. */
router.get('/logout', (req, res, next) => {
    req.user.tokens = req.user.tokens.filter((token) => {
        return token.token !== req.token
    })
    req.user.save()

    if (!req.session) {
        return next()
    }
    return req.session.destroy((err) => {
        if (!err) {
            return res.redirect('/')
        }
        next(new Error(err))
    })
})

module.exports = router