const express = require('express')
const router = express.Router()
const User = require('../models/user')
const Recipe = require('../models/recipe')
const bodyParser = require("body-parser")

router.use(bodyParser.json())

/* GET home page. */
router.get('/', (req, res) => {
  let userId = ''
  if (req.user) userId = req.user._id
  Recipe.find().then((recipes) => {
    User.find().then((users) => {
      res.render('index', {
        title: "Recipes | recipe.io",
        recipes: recipes,
        hasToken: !!req.session.token,
        users: users,
        userId: userId.toString()
      })
    })
  })
})

/* GET login page. */
router.get('/login', (req, res) => {
  res.render('user/login', {
    title: "Login | recipe.io",
    hasToken: !!req.session.token,
    error: null,
    failReason: null
  })
})

/* GET user profile page. */
router.get('/profile/:username', (req, res) => {
  User.findOne({username: req.params.username}).then(user => {
    Recipe.find({userId: user._id.toString()}).then(recipes => {
      let userId = ''
      if (req.user) userId = req.user._id
      res.render('user/profile', {
        title: user.username + "'s Profile | recipe.io",
        hasToken: !!req.session.token,
        recipes: recipes,
        user: user,
        userId: userId.toString()
      })
    })
  })
})

/* POST a register request. */
router.post('/register', (req, res) => {
  const user = new User(req.body)
  user.save().then((result) => {
    return req.session.save((error) => {
      res.redirect('/login')
    })
  }, (error) => {
    res.redirect('/login/registration_failed')
  })
})

/* POST a login request. */
router.post('/login', async (req, res) => {
  try {
    const user = await User.findByCredentials(req.body.login, req.body.password)
    const token = await user.generateToken()

    if (user) {
      req.session.user = user
      req.session.token = token
      return req.session.save((err) => {
        res.redirect('/')
      })
    }
  } catch (error) {
    res.redirect('/login/login_failed')
  }
})

/* GET login page after failed login. */
router.get('/login/:failed', (req, res) => {
  let errorMsg = ''
  let failReason = ''
  switch (req.params.failed){
    case 'login_failed':
      errorMsg = 'Wrong username or password!'
      failReason = 'login'
      break;
    case 'registration_failed':
      errorMsg = 'Registration failed!'
      failReason = 'registration'
      break;
    default:
      console.log("Should not happen")
  }

  res.render('user/login', {
    title: "Login | recipe.io",
    hasToken: !!req.session.token,
    error: errorMsg,
    failReason : failReason
  })
})

/* GET credits page. */
router.get('/credits', (req, res) => {
  res.render('credits', {
    title: "Credits | recipe.io",
    hasToken: !!req.session.token,
  })
})

module.exports = router