const express = require('express')
const path = require('path')
const createError = require('http-errors')
const bodyParser = require('body-parser')
const sassMiddleware = require('node-sass-middleware');

const mongoose = require('mongoose')
const session = require('express-session')
const MongoClient = require('connect-mongodb-session')(session)
const uri = "mongodb+srv://Marek:RQcGM2Q2JqyBpj@recepty.rhe6t.mongodb.net/recipes?retryWrites=true&w=majority";

const User = require('../models/user')
const publicRouter = require('../routes/public')
const userRouter = require('../routes/user')

const app = express()

const mongodbSessionStorage = new MongoClient({
  uri: uri,
  collection: 'sessions',
})

app.use(
    session({
        secret: 'Foobar',
        resave: false,
        saveUninitialized: false,
        store: mongodbSessionStorage
    })
)

app.use((req, res, next) => {
    if (!req.session.user) {
        return next()
    }
    User.findOne({username: req.session.user.username})
        .then((user) => {
            req.user = user
            next()
        })
        .catch((err) => console.log(err))
})

app.use(bodyParser.urlencoded({ extended: true }))

// view engine setup
app.set('views', path.join(__dirname, '../views'))
app.set('view engine', 'ejs')

/* SASS Middleware */
app.use(sassMiddleware({
  src: path.join(__dirname, '../public'),
  dest: path.join(__dirname, '../public'),
  indentedSyntax: false, // true = .sass and false = .scss
  sourceMap: true
}))

app.use(express.static(path.join(__dirname, '../public')))

app.use(publicRouter)
app.use(userRouter)
app.use('/css', express.static(path.join(__dirname, '../node_modules/bootstrap/dist/css')))
app.use('/css', express.static(path.join(__dirname, '../node_modules/bootstrap-icons/font/')))
app.use('/js',  express.static(path.join(__dirname, '../node_modules/bootstrap/dist/js')))
app.use('/js',  express.static(path.join(__dirname, '../node_modules/jquery/dist')))
app.use('/js',  express.static(path.join(__dirname, '../node_modules/packery/dist')))
app.use('/js',  express.static(path.join(__dirname, '../node_modules/minigrid/dist')))
app.use('/js',  express.static(path.join(__dirname, '../public/js')))

mongoose
    .connect(uri, {
        useNewUrlParser: true,
        useCreateIndex: true,
        useUnifiedTopology: true
    })
    .then(() => {
      app.listen(3000)
    })
    .catch((err) => console.log(err))

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404))
})

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message
  res.locals.error = req.app.get('env') === 'development' ? err : {}

  // render the error page
  res.status(err.status || 500)
  res.render('error', {
      title: err.status + ' - ' + err.message,
      hasToken: !!req.session.token
  })
})

module.exports = app